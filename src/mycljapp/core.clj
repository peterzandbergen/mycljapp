(ns mycljapp.core
  (:gen-class))

(defn ways [steps possible] 
  ; (println steps possible)
  (reduce 
    (fn 
      ([] (println "case 1") 0)
      ([val s] 
        (cond 
            ; (== s steps) (do (println s "==" steps) (+ val 1))
            (== s steps) (do 
              ; (println "reached the top taking " s " steps") 
              (+ val 1))
            (< s steps) (do 
              ; (println "taking another step:" s " to reach " steps) 
              (+ val (ways (- steps s) possible)))
            :else val
        )
      )
    )
    0 possible
  )
)
       

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (do 
    (println "starting")
    (println "number of ways" (ways 100 #{1 3 4}))
  )
)
  ; (println (ways 2 [1 2])))
